/**@author Wenberg 
 * GUI is implemented through the fx markup language file. 

 * Generated GUITemplate.fxml through the Oracle JavaFX Scene Builder 2.0
 * @link http://www.oracle.com/technetwork/java/javase/downloads/sb2download-2177776.html
 * 
 * Hierarchy of UI Objects:
 * 
 * >TitledPane: Creates backdrop for layout
 * >>AnchorPane: Creates box that offsets the layout but is anchored so that it can't move
 * >>>ScrollPane: Creates a slider window to scroll a list of files that may be inputed
 * >>>ScrollBar: helps the slider window 
 * >>ToolBar: a nice place to put tabs and place buttons
 * >>>Button: to select or browse files, can be placed in toolbar
 * >>Accordion: creates accordion style box with three sections that can be opened.
 * >>>TitledPane:Creates backdrop for layout
 * >>>>AnchorPane:Creates box that offsets the layout but is anchored so that it can't move
 * >>>TitledPane:Creates backdrop for layout
 * >>>>AnchorPane:Creates box that offsets the layout but is anchored so that it can't move
 * 
 * 
 * 
 * 
 * @author steinmee -- Professor
 * @author Hagen -- Tutor 
 * @author Wenberg -- Coder
 */

	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Parent root = FXMLLoader.load(getClass().getResource("GUITemplate.fxml"));
		
		Scene scene = new Scene(root);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	/*
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		*/
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
