import java.io.File;
import java.io.IOException;
import java.util.*;




/**
* The "WordCount" program simply counts the words in a file application. 
*
* @author  Mack Wenberg -- Coder
* @version 1.0
* @since   2016-9-30
*/

public class WordCount
{
	/**
	 * 
	 * @param f : File to be passed into WordCount; it will scan the words that are tokenized in the file, create a list of all words,
	 * , and tally repeats of each word. 
	 * 
	 * @param v Tokenfrequency, an extension of HashMap designed to 'list;/ store each word (as a key) associating the number of occurences 
	 * as a value. 
	 * 
	 * @throws IOException meant to throw an exception if file is corrupt or not found.
	 */
	public WordCount(File f, Tokenfrequency v) throws IOException//
	{
		Scanner MyScanner = new Scanner(f); //creating new scanner that takes in file f which is passed in
		Tokenfrequency MyHashmap = v; //creating new Hashmap called MyHashMap 
		while(MyScanner.hasNext())
		{
			String S = MyScanner.next(); // 
			if(MyHashmap.containsKey(S)) // proceed if string S is in our hashmap
			{
				int temp = MyHashmap.get(S);
				MyHashmap.replace(S, temp, temp++);//keeps track of temp
			}
			else//if s isn't in our hashmap
			{
				MyHashmap.put(S, 1); 
				MyHashmap.KeyChain.add(S);
			}
		}
		MyScanner.close();// closes the "MyScanner" scanner when program is finished. 
	}
}
