import java.util.*;
import java.util.HashMap;

/**
* The "Tokenfrequency" program implements the word counter program
*
* @author  Mack Wenberg -- Coder
* @version 1.0
* @since   2016-9-30
*/

@SuppressWarnings("serial")
public class Tokenfrequency extends HashMap<String, Integer>//


/**
 * Also includes: ArrayList<String> keyChain : 
 * 
 * an array to store all of the keys (in this hashmap) as Strings.
 * 
 * 
 */
{
	public ArrayList<String> KeyChain = new ArrayList<String>();//
	
	/**
	 * Override toString for the HashMap "tokenfrequency" 
	 * 
	 * Iterates through the keychain to grab keys (WORDS), pull associated values 
	 * (FREQUENCIES, or how often the word occurs as an integer), and print them
	 * together. 
	 */
	@Override
	public String toString()
	{
		String S = ""; //place holder string to return later.
		int si = KeyChain.size();//grab and store size of this.keyChain
	
		for(int i = 0; i<si; i++)
		{
			String disKey = KeyChain.get(i);//create string, place key out of array. 
			
			int word_freq = this.get(disKey);// value tied to this key (the frequency with this word).
			
			int a = 46 - disKey.length();//number of spaces to be indented
			for(int j = 0; j < a; j++)//indentation loop 
			{
				disKey += "";//concatenates one space per iteration
				
			}
			S += (disKey+ " " + word_freq + "\n");//concatenates WORD, indent, FREQ, new line
		}
		return S; //returns the entire String of hashmap/tokenfrequecy contents
		
	}
	
}